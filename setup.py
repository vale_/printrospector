#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup


setup(
    name="printrospector",
    author="Valentin B.",
    author_email="valentin.be@protonmail.com",
    url="https://gitlab.com/vale_/printrospector",
    license="MIT",
    description="CLI utility to view serialized ObjectProperty state as XML",
    project_urls={
        "source": "https://gitlab.com/vale_/kronos",
    },
    version="0.2.0",
    include_package_data=True,
    extras_require={"speedups": ["lxml"]},
    python_requires=">=3.6.0",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    entry_points={"console_scripts": ["printrospector = printrospector.__main__:main"]},
)
