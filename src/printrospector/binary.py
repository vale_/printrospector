import re
from typing import Any, Dict, Iterable, Optional, Tuple, Union

from .bit_buffer import BitBuffer
from .bitint import read_bitint
from .object import DynamicObject
from .type_cache import Property, PropertyClass, TypeCache

GENERIC_TYPE_RE = re.compile(r".+<([\w\s:\*]+)>")

# Flag bits used by the serializer.
FLAG_STATEFUL_FLAGS = 1 << 0
FLAG_COMPACT_LENGTH_PREFIXES = 1 << 1
FLAG_HUMAN_READABLE_ENUMS = 1 << 2
FLAG_COMPRESS_DATA = 1 << 3
FLAG_REQUIRE_OPTIONAL_PROPERTIES = 1 << 4

_TYPE_FMTS = {
    "char": "<b",
    "unsigned char": "<B",
    "short": "<h",
    "unsigned short": "<H",
    "wchar_t": "<H",
    "int": "<i",
    "unsigned int": "<I",
    "long": "<i",
    "unsigned long": "<I",
    "float": "<f",
    "double": "<d",
    "__int64": "<q",
    "unsigned __int64": "<Q",
    "gid": "<Q",
    "union gid": "<Q",
}

# type_name: (bit_width, signed)
_BITINT_LOOKUP = {}
for i in range(2, 8):
    _BITINT_LOOKUP[f"bi{i}"] = (i, True)
    _BITINT_LOOKUP[f"bui{i}"] = (i, False)
_BITINT_LOOKUP["s24"] = (24, True)
_BITINT_LOOKUP["u24"] = (24, False)


class BinarySerializer:
    def __init__(self, type_cache: TypeCache, serializer_flags: int, exhaustive: bool):
        self.type_cache = type_cache
        self.serializer_flags = serializer_flags
        self.exhaustive = exhaustive

    def _read_length_prefix(self, buf: BitBuffer, is_str: bool):
        if self.serializer_flags & FLAG_COMPACT_LENGTH_PREFIXES != 0:
            bits = (
                buf.read_bits(31, False)
                if buf.read_bit()
                else buf.read_bits(7, False)
            )
            return sum(b << i for i, b in enumerate(bits))
        return buf.read("<H") if is_str else buf.read("<I")

    def _read_enum_variant(self, buf: BitBuffer) -> Union[str, int]:
        if self.serializer_flags & FLAG_HUMAN_READABLE_ENUMS != 0:
            width = self._read_length_prefix(buf, True)
            return buf.read_bytes(width).decode("utf-8")
        return buf.read("<I")

    def deserialize(
        self, data: bytes, property_mask: int = 0x18
    ) -> Optional[DynamicObject]:
        bits = BitBuffer(data)
        bits.seek_to_bit(0)

        if self.serializer_flags & FLAG_STATEFUL_FLAGS != 0:
            self.serializer_flags = bits.read("<I")

        if self.serializer_flags & FLAG_COMPRESS_DATA != 0 and bits.read_bit():
            new_size = bits.read("<I") << 3
            bits.decompress_state()
            assert len(bits) == new_size, "size mismatch (decompression)"

        return self.deserialize_object(bits, property_mask)

    def deserialize_object(
        self, bits: BitBuffer, property_mask: int
    ) -> Optional[DynamicObject]:
        object_hash = bits.read("<I")
        if object_hash == 0:
            return None

        object_size = bits.read("<I") - 32 if self.exhaustive else None
        current_bit_size = len(bits)

        property_class = self.type_cache.get_with_hash(object_hash)
        result = DynamicObject(property_class.name)
        for name, value in self.deserialize_properties(
            bits, object_size, property_class, property_mask
        ):
            result[name] = value

        if object_size:
            assert current_bit_size - len(bits) == object_size, "size mismatch (object)"

        return result

    def deserialize_properties(
        self, bits: BitBuffer, object_size: Optional[int], cls: PropertyClass, mask: int
    ) -> Iterable[Tuple[str, Any]]:
        if self.exhaustive:
            while object_size > 0:
                old_buffer_len = len(bits)
                property_size = bits.read("<I")

                property_hash = bits.read("<I")
                property = cls.property_from_hash(property_hash)
                if not property:
                    raise KeyError(
                        f"Cannot identify for property for hash: {property_hash}"
                    )

                value = self.deserialize_property(bits, property, mask)

                assert (
                    old_buffer_len - len(bits) == property_size
                ), "size mismatch (property)"

                object_size -= property_size

                yield property.name, value
        else:
            for property in filter(lambda p: p.flags & mask == mask, cls):
                yield property.name, self.deserialize_property(bits, property, mask)

    def deserialize_property(self, bits: BitBuffer, property: Property, mask: int) -> Any:
        if property.is_optional():
            if not bits.read_bit():
                if self.serializer_flags & FLAG_REQUIRE_OPTIONAL_PROPERTIES != 0:
                    raise ValueError("Missing property value where required")
                return None

        is_dynamic = property.is_dynamic()
        elements = self._read_length_prefix(bits, False) if is_dynamic else 1
        ty = property.type
        result = []
        for _ in range(elements):
            if property.is_enum() or property.is_scoped_enum():
                variant = self._read_enum_variant(bits)
                result.append(property.decode_enum_variant(variant))
            elif ty in _TYPE_FMTS:
                result.append(bits.read(_TYPE_FMTS[ty]))
            elif ty in _BITINT_LOOKUP:
                nbits, signed = _BITINT_LOOKUP[ty]
                result.append(read_bitint(bits, nbits, signed=signed))
            elif ty == "bool":
                result.append(bits.read_bit())
            elif ty in ("std::string", "char*"):
                width = self._read_length_prefix(bits, True)
                string = bits.read(f"{width}s")
                try:
                    result.append(string.decode("utf-8"))
                except UnicodeDecodeError:
                    # In some stupid cases, strings are used to store binary data.
                    # If we cannot decode to UTF-8, just push the raw bytes as-is.
                    result.append(string)
            elif ty in ("std::wstring", "wchar_t*"):
                width = self._read_length_prefix(bits, True) * 2
                wstring = bits.read(f"{width}s")
                result.append(wstring.decode("utf-16-le"))
            elif ty == "class Color":
                result.append(
                    (bits.read("<B"), bits.read("<B"), bits.read("<B"), bits.read("<B"))
                )
            elif ty == "class Vector3D":
                result.append((bits.read("<f"), bits.read("<f"), bits.read("<f")))
            elif ty.startswith(("class Point", "class Size")):
                real_ty = GENERIC_TYPE_RE.match(ty).group(1)
                result.append(
                    (bits.read(_TYPE_FMTS[real_ty]), bits.read(_TYPE_FMTS[real_ty]))
                )
            elif ty.startswith("class Rect"):
                real_ty = GENERIC_TYPE_RE.match(ty).group(1)
                result.append(
                    (
                        bits.read(_TYPE_FMTS[real_ty]),
                        bits.read(_TYPE_FMTS[real_ty]),
                        bits.read(_TYPE_FMTS[real_ty]),
                        bits.read(_TYPE_FMTS[real_ty]),
                    )
                )
            elif ty == "class Euler":
                result.append((bits.read("<I"), bits.read("<I"), bits.read("<I")))
            elif ty == "class Quaternion":
                result.append(
                    (bits.read("<I"), bits.read("<I"), bits.read("<I"), bits.read("<I"))
                )
            elif ty == "class Matrix3x3":
                result.append(
                    (bits.read("<f"), bits.read("<f"), bits.read("<f")),
                    (bits.read("<f"), bits.read("<f"), bits.read("<f")),
                    (bits.read("<f"), bits.read("<f"), bits.read("<f")),
                )
            else:
                result.append(self.deserialize_object(bits, mask))

        return result if is_dynamic else result[0]
