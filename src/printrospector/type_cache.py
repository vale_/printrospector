from typing import Dict, List, Optional, Union

FLAG_PRIVATE = 1 << 6
FLAG_OPTIONAL = 1 << 8
FLAG_ENUM = 1 << 20
FLAG_SCOPED_ENUM = 1 << 21


class Property:
    def __init__(self, name: str, **kwargs):
        self.name = name
        self.type = kwargs.pop("type")
        self.container = kwargs.pop("container")
        self.flags = kwargs.pop("flags")
        self.options = kwargs.get("enum_options", {})
        self.is_pointer = kwargs.pop("pointer")
        self.hash = kwargs.pop("hash")

    def __hash__(self):
        return self.hash

    def is_dynamic(self) -> bool:
        return self.container in ("List", "Vector")

    def get_default(self) -> Optional[str]:
        # TODO: Fault tolerance for the real default values anchored into C++?
        return self.options.get("__DEFAULT")

    def decode_enum_variant(self, variant: Union[str, int]) -> Union[str, int]:
        if self.is_scoped_enum() and isinstance(variant, str):
            variant = variant.split("::")[-1]
            return f"{self.type}::{variant}"

        elif self.is_scoped_enum() and isinstance(variant, int):
            for k, v in self.options.items():
                if int(v) == variant:
                    return f"{self.type}::{k}"
            raise ValueError(f"Unknown enum variant received: {variant}")

        elif self.is_enum() and isinstance(variant, str) and not len(variant) == 0:
            def lookup_or_value(self, variant):
                lookup = self.options[variant]
                try:
                    return int(lookup)
                except ValueError:
                    return int(self.options[lookup])

            result = 0
            for b in map(lambda s: lookup_or_value(self, s), variant.split("|")):
                result |= b
            return result

        else:
            return variant

    def is_private(self) -> bool:
        return self.flags & FLAG_PRIVATE != 0

    def is_optional(self) -> bool:
        return self.flags & FLAG_OPTIONAL != 0

    def is_enum(self) -> bool:
        return self.flags & FLAG_ENUM != 0

    def is_scoped_enum(self) -> bool:
        return self.flags & FLAG_SCOPED_ENUM != 0


class PropertyClass:
    def __init__(self, name: str, properties: List[Property], hash: int):
        self.name = name
        self.properties = properties
        self.hash = hash

        self._hashes_to_names = {
            property.hash: property for property in self.properties
        }

    def __len__(self):
        return len(self.properties)

    def __iter__(self):
        return iter(self.properties)

    def __hash__(self):
        return self.hash

    def property_from_hash(self, hash: int) -> Optional[Property]:
        return self._hashes_to_names.get(hash)


class TypeCache:
    def __init__(self, types: dict):
        classes = [
            PropertyClass(name, [Property(n, **p) for n, p in value["properties"].items()], value["hash"])
            for name, value in types.items()
        ]

        self._types_by_hash = {cls.hash: cls for cls in classes}

    def get_with_hash(self, hash: int) -> Optional[PropertyClass]:
        obj = self._types_by_hash.get(hash)
        if not obj:
            raise ValueError(f"Encountered unknown class definition (hash {hash})!")
        return obj
