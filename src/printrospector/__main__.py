#!/usr/bin/env python3

import argparse
import json
from pathlib import Path

from . import *
from .binary import FLAG_STATEFUL_FLAGS


def main():
    parser = argparse.ArgumentParser(
        "printrospector",
        description="CLI utility to view serialized ObjectProperty state as XML",
    )
    parser.add_argument("input", help="The BINd XML file to deserialize")
    parser.add_argument("--out", help="The file to output the readable XML data to")
    parser.add_argument("--types", required=True, help="Path to type list file")
    parser.add_argument("--pretty", required=False, default=True, help="Pretty-print output")
    args = parser.parse_args()

    out = args.out
    if not out:
        # let pathlib handle path parsing for us
        input_path = Path(args.input)
        out = "Deserialized_" + input_path.name

    with open(args.types, encoding="utf-8") as f:
        type_cache = TypeCache(json.load(f))

    bin = BinarySerializer(type_cache, FLAG_STATEFUL_FLAGS, True)
    with open(args.input, "rb") as f:
        data = f.read()
        if data[:4] != b"BINd":
            raise RuntimeError("Input file does not start with BINd magic")
        obj = bin.deserialize(data[4:])

    if obj:
        xml = XmlSerializer()
        xml.serialize(obj)
        xml.write_to_output(out, args.pretty)


if __name__ == "__main__":
    main()
