import collections
from typing import Any


class DynamicObject(collections.abc.MutableMapping):
    _transform_key = str

    def __init__(self, name: str, **properties):
        self.name = name
        self._properties = properties

    def __getitem__(self, key: str):
        return self._properties[key]

    def __setitem__(self, key: str, value: Any):
        self._properties[key] = value

    def __delitem__(self, key: str):
        del self._properties[key]

    def __iter__(self):
        return iter(self._properties.items())

    def __len__(self):
        return len(self._properties)
