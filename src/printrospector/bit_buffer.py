import struct
from typing import Any, List, Union
import zlib

# An object that is either a list of bits represented as booleans or
# bytes-like, depending on the context it should be used in.
BufferData = Union[List[bool], bytes, bytearray]


class BitBuffer:
    def __init__(self, data: BufferData = b""):
        self._cursor = 0
        self._bit_offset = 0
        self._length = 0

        # Fill the buffer depending on whether we got bits or bytes.
        if isinstance(data, list):
            self.data = bytearray()
            self.write_bits(data, False)
        else:
            self.data = bytearray(data)
            self._cursor = len(self.data)

            # memoization, stop python from having to count it every time we advance
            self._length = len(self.data)

    def __len__(self):
        # make sure self._length is always up-to-date
        return ((self._length - self._cursor - 1) << 3) + (8 - self._bit_offset)

    def _advance_cursor(self):
        self._bit_offset += 1
        if self._bit_offset >= 8:
            self._cursor += 1
            self._bit_offset = 0

    def _realign_to_byte(self):
        if self._bit_offset > 0:
            self._cursor += 1
            self._bit_offset = 0

    def decompress_state(self):
        assert self._bit_offset == 0

        # Decompress the data starting at current buffer position.
        self.data = zlib.decompress(self.data[self._cursor:])
        self._length = len(self.data)

        # Reset the cursor to 0 (which is the new start of the decompressed data).
        self._cursor = 0

    def seek_to_bit(self, nbit: int):
        self._cursor = nbit >> 3
        self._bit_offset = nbit & 7

    def write_bit(self, bit: bool):
        if self._length - 1 < self._cursor:
            self.data.append(0)
            self._length += 1

        if bit:
            self.data[self._cursor] |= 1 << self._bit_offset
        else:
            self.data[self._cursor] &= ~(1 << self._bit_offset)

        self._advance_cursor()

    def write_bits(self, buf: BufferData, realign: bool):
        if realign:
            self._realign_to_byte()

        if not isinstance(buf, list):
            # When we get anything that is not list, we assume we got bytes.
            # Convert these into a list of booleans represnting the bits first.
            buf = [byte & (1 << n) != 0 for byte in buf for n in range(8)]

        for bit in buf:
            self.write_bit(bit)

    def read_bit(self) -> bool:
        if self._length - 1 < self._cursor:
            raise OverflowError("cannot read past the end of the buffer")

        value = self.data[self._cursor] & (1 << self._bit_offset) != 0
        self._advance_cursor()
        return value

    def read_bits(self, nbits: int, realign: bool) -> List[bool]:
        if realign:
            self._realign_to_byte()

        return [self.read_bit() for _ in range(nbits)]

    def read_bytes(self, nbytes: int) -> bytes:
        # This optimization will not work if alignment should be reset
        self._realign_to_byte()
        res = self.data[self._cursor:self._cursor + nbytes]
        self._cursor += nbytes
        return res

    def read(self, fmt: str) -> Any:
        # XXX: This does not work with "??"!
        value = struct.unpack(fmt, self.read_bytes(struct.calcsize(fmt)))
        return value[0] if len(value) == 1 else value
